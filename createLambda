#!/bin/sh

function createLambda() {
  lambda=$1
  
  echo "Creating lambda: $lambda..."
  
  # Create new folder
  mkdir $lambda
  
  # Generate tsconfig.json
  jq -n '{
    "compilerOptions": {
      "strict": true,
      "target": "ESNext",
      "forceConsistentCasingInFileNames": true,
      "module": "NodeNext",
      "outDir": "dist",
      "baseUrl": "src"
    },
    "include": ["src/**/*"]
  }' > $lambda/tsconfig.json
  
  # Generate package.json
  jq -n --arg name "$lambda" --arg description "Lambda that performs $lambda" '{
    name: $name,
    version: "1.0.0",
    description: $description,
    author: "Paul Kimbrel",
    contributors: [{ name: "Paul Kimbrel", email: "pkimbrel@gmail.com" }],
    license: "Private",
    type: "commonjs",
    sideEffects: false,
    files: ["lib", "src"]
  }' > $lambda/package.json
  
  # Copy dependencies and devDependencies from api-common's package.json
  jq -s '.[0] * .[1]' api-common/package.json $lambda/package.json > temp.json && mv temp.json $lambda/package.json
  
  # Add "@pkimbrel/api-common": "../api-common" to dependencies
  jq '.dependencies += {"@pkimbrel/api-common": "../api-common"}' $lambda/package.json > temp.json && mv temp.json $lambda/package.json
  
  # Create src folder and index.ts file
  mkdir $lambda/src
  cat <<EOF >$lambda/src/index.ts
import { buildResponse } from "@pkimbrel/api-common";
import { APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda";

export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  return buildResponse(200, { message: "Hello, world!" });
};
EOF
  
  # Enter the folder and execute npm install
  echo "Installing development dependencies..."
  cd $lambda
  npm install > /dev/null
  cd ..
  
  # Update deploy script
  echo "Updating deploy script..."
  sed -i '' "/^LAMBDAS=/ s/)$/ \"$lambda\")/" deploy
}

createLambda $1