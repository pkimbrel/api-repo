import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";

const client = new DynamoDBClient({ region: "us-east-1" });
const db = DynamoDBDocument.from(client);

export const readAll = async (): Promise<Todo[]> => {
  const result = await db.query({
    TableName: "api-state",
    KeyConditionExpression: "#pk = :pk",
    ExpressionAttributeNames: {
      "#pk": "pk"
    },
    ExpressionAttributeValues: {
      ":pk": "todos"
    }
  });

  const items = (result?.Items || []) as DBTodo[];

  return items.map(({ pk, sk, ...todo }) => ({ id: sk, title: todo.title, completed: todo.completed }));
};

export const buildResponse = (statusCode: number, body: any) => {
  return {
    statusCode,
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json"
    }
  };
};

export interface Todo {
  id: string;
  title: string;
  completed: boolean;
}

export interface DBTodo extends Todo {
  pk: string;
  sk: string;
}
