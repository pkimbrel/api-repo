export declare const readAll: () => Promise<Todo[]>;
export declare const buildResponse: (statusCode: number, body: any) => {
    statusCode: number;
    body: string;
    headers: {
        "Content-Type": string;
    };
};
export interface Todo {
    id: string;
    title: string;
    completed: boolean;
}
export interface DBTodo extends Todo {
    pk: string;
    sk: string;
}
