"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildResponse = exports.readAll = void 0;
const client_dynamodb_1 = require("@aws-sdk/client-dynamodb");
const lib_dynamodb_1 = require("@aws-sdk/lib-dynamodb");
const client = new client_dynamodb_1.DynamoDBClient({ region: "us-east-1" });
const db = lib_dynamodb_1.DynamoDBDocument.from(client);
const readAll = async () => {
    const result = await db.query({
        TableName: "api-state",
        KeyConditionExpression: "#pk = :pk",
        ExpressionAttributeNames: {
            "#pk": "pk"
        },
        ExpressionAttributeValues: {
            ":pk": "todos"
        }
    });
    const items = (result?.Items || []);
    return items.map(({ pk, sk, ...todo }) => ({ id: sk, title: todo.title, completed: todo.completed }));
};
exports.readAll = readAll;
const buildResponse = (statusCode, body) => {
    return {
        statusCode,
        body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json"
        }
    };
};
exports.buildResponse = buildResponse;
