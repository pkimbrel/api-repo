import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocument } from "@aws-sdk/lib-dynamodb";
import { buildResponse, readAll } from "@pkimbrel/api-common";
import { APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda";

const client = new DynamoDBClient({ region: "us-east-1" });
const db = DynamoDBDocument.from(client);

export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const id = event.pathParameters?.id;
  const input = JSON.parse(event.body || "{}");

  const item = {
    pk: "todos",
    sk: id,
    ...input
  };

  await db.put({
    TableName: "api-state",
    Item: item
  });

  const result = await readAll();

  return buildResponse(200, result);
};
