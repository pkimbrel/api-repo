#!/bin/sh

# Check if --help or -? is passed as an argument
if [ "$1" == "--help" ] || [ "$1" == "-?" ]; then
  echo
  echo "Available stages:"
  echo
  echo "copy-common - Copies dependencies and devDependencies from api-common to the lambda"
  echo "audit-fix     - Runs and "audit fix" for the lambdas"
  echo "install       - Installs development tools for the lambdas"
  echo "build         - Builds the lambdas"
  echo "deps          - Installs production dependencies for the lambdas"
  echo "package       - Packages the lambdas"
  echo "issue         - Issues the lambdas"
  echo
  exit 0
fi

COMMON="api-common"
LAMBDAS=("api-todos-get" "api-todo-put" "api-todo-delete")
HASH_FILE="hashes.txt"

AWS_PROFILE=personal-dev

function hash_directory() {
  dir=$1
  git ls-files $dir | tr '\n' '\0' | xargs -0 git hash-object | git hash-object --stdin
}

function copy_dev_deps() {
  lambda=$1
  
  echo "Copying dependencies and devDependencies from $COMMON to $lambda"
  
  jq -s '
    reduce .[] as $item ({};
      . * $item |
      .dependencies += ($item.dependencies // {}) |
      .dependencies += {"@pkimbrel/api-common": "../api-common"} |
      .devDependencies += ($item.devDependencies // {})
  )' $COMMON/package.json $lambda/package.json > temp.json && mv temp.json $lambda/package.json
}

function audit_fix() {
  lambda=$1
  
  echo "Running 'audit fix' for $lambda..."
  cd $lambda
  npm audit fix > /dev/null
  cd ..
}

function install() {
  lambda=$1
  
  echo "Installing development tools for $lambda..."
  cd $lambda
  npm install > /dev/null
  cd ..
}

function build() {
  lambda=$1
  
  echo "Building $lambda..."
  cd $lambda
  npm install > /dev/null
  npx tsc --build > /dev/null
  npx tsc-alias > /dev/null
  cd ..
}

function deps() {
  lambda=$1
  
  echo "Installing production dependencies for $lambda..."
  cd $lambda
  cp package.json ./dist
  cd dist
  jq '.dependencies |= with_entries(if .value | startswith("../") then .value |= "../" + . else . end)' package.json > temp.json && mv temp.json package.json
  npm install --omit=dev --prefix . > /dev/null
  cd ../..
}

function package() {
  lambda=$1
  
  echo "Packaging $lambda..."
  
  cd $lambda
  version=$(jq -r '.version' package.json)
  
  rm -f dist/*.zip
  mkdir pkg
  cp -r -L dist/ pkg/
  cd pkg
  zip -r ../dist/$lambda-$version.zip . > /dev/null
  cd ..
  rm -r pkg
  cd ..
}

function issue() {
  lambda=$1
  current_hash=$2
  
  echo "Issuing $lambda..."
  cd $lambda
  version=$(jq -r '.version' package.json)
  aws --no-cli-pager lambda update-function-code --region us-east-1 --function-name $lambda --zip-file fileb://./dist/$lambda-$version.zip > /dev/null
  cd ..
  
  # Update the hash in the file
  sed -i "" "/$lambda/d" $HASH_FILE
  echo "$lambda $current_hash" >> $HASH_FILE
  
}

function deploy_lambda() {
  lambda=$1
  target=$2
  
  echo "--------------------------------------------------"
  echo
  echo "Processing deployment for $lambda..."
  
  # Compute the current hash of the lambda
  current_hash=$(hash_directory $lambda)
  
  # Check if the hash file exists and the lambda's hash is in the file
  if [ -f "$HASH_FILE" ] && grep -q "$lambda" "$HASH_FILE"; then
    # Get the stored hash of the lambda
    stored_hash=$(grep "$lambda" $HASH_FILE | cut -d ' ' -f 2)
  else
    # Set the stored hash to a default value
    stored_hash="missing"
  fi
  
  # If the hashes are different, proceed with the deployment
  if [ "$current_hash" != "$stored_hash" ] || [ "$target" = "$lambda" ] || [ "$target" = "all" ]; then
    echo "Deployment for $lambda STARTED."
    
    # Perform the specified stages of deployment
    for stage in "${stages[@]}"; do
      case $stage in
        --copy-common)
          copy_dev_deps $lambda
        ;;
        --audit-fix)
          audit_fix $lambda
        ;;
        --install)
          install $lambda
        ;;
        --build)
          build $lambda
        ;;
        --deps)
          deps $lambda
        ;;
        --package)
          package $lambda
        ;;
        --issue)
          issue $lambda $current_hash
        ;;
        *)
          echo "Unknown stage: $stage"
        ;;
      esac
    done
  else
    echo "Deployment for $lambda SKIPPED."
  fi
  
  echo
}

function common_rebuild_tag() {
  echo "Rebuilding common library..."
  
  cd $COMMON
  npm install > /dev/null
  npx tsc --build > /dev/null
  npx tsc-alias > /dev/null
  cd ..
}

# Get the stages from the command-line arguments
stages=("${@:1:$#-1}")

# Get the target from the last command-line argument
target=${!#}

# Check if target is 'common'
if [ "$target" == "common" ]; then
  # Run common rebuild tag
  common_rebuild_tag
  
  # Set target to 'all'
  target="all"
  
  # Set stages to the standard set
  stages=(--install --build --deps --package --issue)
fi

# If no stages are provided, perform all stages
if [ ${#stages[@]} -eq 0 ]; then
  stages=(--install --build --deps --package --issue)
fi

for LAMBDA in "${LAMBDAS[@]}"
do
  if [ ! -d "$LAMBDA" ]; then
    echo "Error: $LAMBDA does not exist."
    exit 1
  fi
  
  deploy_lambda $LAMBDA $target "${stages[@]}"
done
